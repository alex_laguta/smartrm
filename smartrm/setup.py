from setuptools import setup, find_packages

setup(
    name='SmartRM',
    version='1.0',
    author='Alexey Laguta',
    author_email='lehalag@gmail.com',
    packages=find_packages(),
    include_package_data=True,
    entry_points={
    'console_scripts': ['SmartRM = src.parser:main']
    }
)