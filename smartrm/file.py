import os
import time


class File(object):
    def __init__(self, path):
        self.abspath = os.path.abspath(path)
        self.path = os.path.relpath(path)
        self.name = os.path.basename(path)

    def is_exist(self):
        return os.path.exists(self.abspath)

    def is_file(self):
        return os.path.isfile(self.abspath)

    def is_link(self):
        return os.path.islink(self.abspath)

    def is_dir(self):
        return os.path.isdir(self.abspath)

    def get_size(self):
        return os.path.getsize(self.abspath)

    @staticmethod
    def get_last_modified_time():
        current_time = time.localtime()
        return time.strftime('%H:%M:%S %d.%m.%y', current_time)
