import sys
import argparse
import os
import pickle
import logging

import logger
import smrm
import config
from trash import Trash
from smrmexceptions import *


class Parser(object):

    def __init__(self):
        self.path_transmissions = None
        self.transmissions = None
        self.init_transmissions()

        self.parser = argparse.ArgumentParser()
        self.parser.add_argument('files', nargs='*')
        self.parser.add_argument('-t', '--trash', action='store_true')
        self.parser.add_argument('-d', '--dir', action='store_true')
        self.parser.add_argument('-r', '--recursive', action='store_true')
        self.parser.add_argument('--regex', default='')
        self.parser.add_argument('--dry-run', dest='dry_run', action='store_true')

        self.parser.add_argument('-R', '--restore', default='')
        self.parser.add_argument('-t', '--transmissions', action='store_true')

        self.policy_group = self.parser.add_mutually_exclusive_group()

        self.policy_group.add_argument('-i', '--interactive', action='store_true')
        self.policy_group.add_argument('-f', '--force', action='store_true')
        self.policy_group.add_argument('--check-system', dest='check_system', action='store_true')
        self.policy_group.add_argument('-s', '--silent', action='store_true')

        self.parser.add_argument('--trash-show', dest='trash_show', action='store_true')
        self.parser.add_argument('--trash-clear', dest='trash_clear', action='store_true')

        self.parser.add_argument('--given-space', dest='given_space', action='store', type=int)
        self.parser.add_argument('--auto-clear-size', dest='maxsize', action='store')
        self.parser.add_argument('--auto-clear-time', dest='maxtime', action='store')

        self.parser.add_argument('--path-config', dest='path_config', default='')
        self.parser.add_argument('--log-file', dest='log_file', default='')
        self.parser.add_argument('--local', nargs='*', default=[])
        self.parser.add_argument('--global', dest='glob', nargs='*', default=[])
        self.parser.add_argument('--format', dest='form', default='config')

    def init_transmissions(self):
        self.path_transmissions = os.path.join(os.path.dirname(__file__), '../transmissions')
        try:
            with open(self.path_transmissions, 'r') as file:
                self.transmissions = pickle.load(file)
        except EOFError:
            self.transmissions = list()

    def parse(self):
        arg = self.parser.parse_args()

        try:
            logger = self.get_logger(arg)
        except Exception:
            sys.exit(1)

        try:
            config = self.get_config(arg)
            trash_path = config.trash_path
            trash = Trash(trash_path)

            if arg.trash_show:
                trash.show()
            elif arg.trash_clear:
                trash.clear_trash()
            elif arg.restore:
                transmission = self.get_transmission(arg.restore)
                index = 0
                for i in range(len(self.transmissions)):
                    if self.transmissions[i].id == transmission.id:
                        index = i
                        break
                try:
                    smrm.restore(transmission, trash, arg.dry_run)
                except RMException as exc:
                    logger.error(exc.message)
                else:
                    del self.transmissions[index]

                if not arg.dry_run:
                    with open(self.path_transmissions, 'w') as file:
                        pickle.dump(self.transmissions, file)
            elif arg.transmissions:
                transmissions = self.transmissions
                if trash:
                    transmissions = [tr for tr in self.transmissions if tr.trash_path == trash.trash_path]

                for transmission in transmissions:
                    transmission.show()
                    print '-' * 10
            elif arg.given_space:
                trash.given_space = arg.given_space
                trash.auto_clear()
            elif arg.maxsize:
                trash.maxsize = arg.maxsize
                trash.auto_clear()
            elif arg.maxtime:
                trash.maxtime = arg.maxtime
                trash.auto_clear()
            else:
                self.setup_policy(arg)
                self.remove_files(arg, trash, logger, arg.silent)

            sys.exit(0)
        except RMException as exc:
            if not arg.silent:
                print exc.message
            sys.exit(1)
        except Exception:
            if not arg.silent:
                print 'System error'
            sys.exit(1)

    def get_logger(self, arg):
        if arg.log_file:
            logger.log_init(arg.log_file)
        else:
           logger.log_init()

        return logging.getLogger('SmartRM_logger')

    def get_config(self, arg):
        if arg.path_config:
            configuration = config.get_by_file(arg.path_config, form=arg.form)
        else:
            configuration = config.get_by_user()

        for line in arg.glob:
            option, value = line.split('=')
            if '.' in option:
                option, suboption = option.split('.')
                if value.isdigit():
                    value = int(value)

                if option not in dir(configuration):
                    setattr(configuration, option, dict())

                configuration.__dict__[option][suboption] = value
            else:
                setattr(configuration, option, value)
                configuration.option = value

        configuration.trash_path = os.path.abspath(configuration.trash_path)

        if not arg.path_config:
            config.save(configuration, os.path.expanduser('~/.trashconfig'))
        else:
            config.save(configuration, arg.path_config, form=arg.form)

        for line in arg.local:
            option, value = line.split('=')
            if '.' in option:
                option, suboption = option.split('.')
                if value.isdigit():
                    value = int(value)

                if option not in dir(configuration):
                    setattr(configuration, option, dict())

                configuration.__dict__[option][suboption] = value
            else:
                setattr(configuration, option, value)

        return configuration

    def get_transmission(self, transmission_id):
        transmissions = [tr for tr in self.transmissions if str(tr.id).startswith(transmission_id)]

        if len(transmissions) > 1:
            raise RMException(transmission_id + ' there are too many transmissions')
        elif len(transmissions) == 0:
            raise RMException(transmission_id + ' transmission not found')
        else:
            return transmissions[0]

    def setup_policy(self, arg):
        if arg.silent or arg.force:
            smrm.set_remove_policy('force')
        elif arg.interactive:
            smrm.set_remove_policy('interactive')
        else:
            smrm.set_remove_policy('check_system')

    def remove_files(self, arg, trash, logger, silent):
        for file in arg.files:
            try:
                if os.path.isfile(file) or os.path.islink(file):
                    smrm.remove_file(file, trash, arg.dry_run)
                elif os.path.isdir(file):
                    if arg.regex:
                        smrm.remove_by_regex(file, arg.regex, trash, arg.dry_run, silent)
                    elif arg.recursive:
                        smrm.remove_tree(file, trash, arg.dry_run, )
                    elif arg.dir:
                        smrm.remove_directory(file, trash, arg.dry_run)
                else:
                    raise RMException(file + ' not a file or directory')
            except RMException as exc:
                if not silent:
                    if logger:
                        logger.error(exc.message)
                    print exc.message

        if not arg.dry_run:
            if arg.trash and smrm.transmission.files:
                transmission = smrm.transmission

                transmission.trash_path = trash.trash_path
                self.transmissions.append(transmission)
                logger.info(str(transmission.id) + ' : operation added')

            with open(self.path_transmissions, 'w') as transmissions_file:
                pickle.dump(self.transmissions, transmissions_file)


def main():
    parser = Parser()
    parser.parse()

if __name__ == '__main__':
    main()
