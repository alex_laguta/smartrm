import os
import re
import logging

import policy
from trash import Trash
from smrmexceptions import *
from logger import log_init
from transmission import Transmission
from file import File


logger = logging.getLogger('SmartRM_logger')
transmission = Transmission()


def main():
    log_init()
    trash = Trash()
    # remove_by_regex('/home/student/PycharmProjects/', '([a-zA-Z0-9]+)3.py', trash)
    remove('/home/student/PycharmProjects/22.py', trash)
    transmission.show()


def remove(path, trash=None, dry_run=False):
    if os.path.isfile(path):
        return remove_file(path, trash, dry_run)
    if os.path.isdir(path):
        return remove_tree(path, trash, dry_run)


def remove_file(path, trash=None, dry_run=False):
    global transmission

    if trash:
        if not dry_run:
            transmission.add_file(File(path))
            filename_in_trash = trash.move_to_trash(path)
            transmission.add_name(filename_in_trash)
        elif path.startswith(trash.trash_path):
            raise RMException('Try to delete files in trash')
        else:
            transmission.add_name(File(path).name)
            transmission.add_file(File(path))
    else:
        raise RMException('No trash to proceed the operation')

    logger.info(path + ' successfully deleted')


def remove_directory(path, trash=None, dry_run=False):
    if os.listdir(path):
        raise DirectoryIsNotEmpty(filename=path)
    else:
        remove_file(path, trash, dry_run)


def remove_tree(path, trash=None, dry_run=False, silent=False):
    for root, dirs, files in os.walk(path):
        for file in files:
            try:
                subpath = os.path.join(root, file)
                remove_file(subpath, trash, dry_run)
            except RMException as exc:
                logger.error(exc.message)
                if not silent:
                    print exc.message

        for directory in dirs:
            subpath = os.path.join(root, directory)
            if os.path.exists(subpath) and not os.access(subpath, os.R_OK):
                msg = subpath + ' Permission denied'
                logger.error(msg)
                if not silent:
                    print msg
        try:
            remove_directory(root, trash, dry_run)
        except RMException as exc:
            logger.error(exc.message)
            if not silent:
                print exc.message


def remove_by_regex(path, pattern, trash=None, dry_run=False, silent=False):
    for root, dirs, files in os.walk(path):
        for filename in files:
            subpath = os.path.join(root, filename)
            if _match_regex(filename, pattern):
                try:
                    remove_file(subpath, trash, dry_run)
                except RMException as exc:
                    logger.error(exc.message)
                    if not silent:
                        print exc.message


def restore(restore_transmission=None, filename=None, trash=None, dry_run=False):
    if filename:
        restore_file(filename, trash, dry_run)
        restore_transmission.remove_file(filename)
    elif restore_transmission:
        try:
            logger.info('Transmission #' + str(restore_transmission.id) + ' is restoring...')
            for name in restore_transmission.filenames:
                restore_file(name, trash, dry_run)
            logger.info('Transmission #' + str(restore_transmission.id) + ' restored successfully')
        except RMException as exc:
            logger.error(exc.message)


def restore_file(filename, trash=None, dry_run=False):
    try:
        if not dry_run:
            trash.restore(filename)
        else:
            logger.info(filename + ' Trying to restore')

            if not trash.is_in_trash(filename):
                raise RMException(filename + ' not found')

            logger.info(filename + ' Restored successfully')
    except RMException as exc:
        logger.error(exc.message)


def set_remove_policy(name):
    global remove_directory
    global remove_file

    remove_directory = policy.remove_policy(name)(remove_directory)
    remove_file = policy.remove_policy(name)(remove_file)


def _match_regex(filename, pattern):
    res = re.match(pattern, os.path.basename(filename))
    if res is not None:
        res = res.group(0)

    return res == os.path.basename(filename)


if __name__ == '__main__':
    main()
