import os

from smrmexceptions import *


def remove_policy(name):
    def interactive(func):
        def wrapper(path, trash, *args):
            if not os.path.exists(path):
                raise RMException(path + 'not found')

            if not os.access(path, os.R_OK) and os.path.isdir(path):
                raise RMException(path + 'permission denied')

            type_of_path = 'directory' if os.path.isdir(path) else 'file'

            print 'Do you want to move', type_of_path, \
                path, 'to trash?'

            answer = raw_input()
            if answer == 'y':
                return func(path, trash, *args)
            else:
                return 1

        return wrapper

    def check_if_system(func):
        def wrapper(path, trash, *args):
            if not os.access(path, os.R_OK) and os.path.isdir(path):
                raise RMException(path + ' permission denied')

            if not os.access(path, os.R_OK):
                print 'Do you want to move system file', \
                    path, 'to trash?'

                answer = raw_input()
                if answer == 'y':
                    return func(path, trash, *args)
                else:
                    return 1
            else:
                return func(path, trash, *args)

        return wrapper

    def force(func):
        def wrapper(path, trash, *args):
            return func(path, trash, *args)

        return wrapper

    if name == 'interactive':
        return interactive
    if name == 'check_if_system':
        return check_if_system
    if name == 'force':
        return force


def restore_policy(name):
    def select(transmission):
        collisions = []
        for file in transmission.files:
            if os.path.exists(file.path):
                collisions.append(file)

        if collisions:
            print 'Detected {0} collisions: '.format(len(collisions))
            for file in collisions:
                print ' ' * 5 + file.path

            print 'Do you want to restore files?'
            if raw_input() != 'y':
                raise RMException(str(transmission.id) + ' collisions were found')

    def cancel(transmission):
        for file in transmission.files:
            if os.path.exists(file.path):
                raise RMException(str(transmission.id) + ' collisions were found')

    def replace(transmission):
        pass

    if name == 'select':
        return select
    elif name == 'cancel':
        return cancel
    elif name == 'replace':
        return replace
