import logging
import os

from smrmexceptions import *


def log_init(filename=None):
    logger = logging.getLogger('SmartRM_logger')
    logger.setLevel(logging.DEBUG)

    if filename:
        if os.path.exists(filename):
            if not os.path.isfile(filename):
                raise RMException(filename + ' invalid log file')

            if not os.access(filename, os.W_OK):
                raise RMException(filename + ' access denied')

        handler = logging.FileHandler(filename, mode='w')
    else:
        handler = logging.StreamHandler()

    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s')

    handler.setFormatter(formatter)

    logger.addHandler(handler)
