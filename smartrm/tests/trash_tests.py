import os
import os.path
import unittest
import time
import shutil
import tempfile

import src.config as cf
from src.trash import Trash
from src.smrmexceptions import RMException


class TestTrash(unittest.TestCase):
    def setUp(self):
        config = cf.get_default()
        trash_path = tempfile.mkdtemp()
        config.trash_path = trash_path
        self.trash = Trash(trash_path)

    def tearDown(self):
        shutil.rmtree(self.trash.trash_path)

    def test_add_one(self):
        file = tempfile.NamedTemporaryFile()
        self.trash.move_to_trash(file.name)
        self.assertFalse(os.path.exists(file.name), 'The file was not removed from the directory')

        file_trash_path = os.path.join(self.trash.trash_files_path, os.path.basename(file.name))
        self.assertTrue(os.path.exists(file_trash_path), 'The file was not removed to the trash')

    def test_restore_file(self):
        file = tempfile.NamedTemporaryFile()

        self.trash.move_to_trash(file.name)
        self.trash.restore(os.path.basename(file.name))

        self.assertTrue(os.path.exists(file.name), 'The file was not restored')

        if os.path.exists(self.trash.trash_files_path):
            self.assertEqual(os.listdir(self.trash.trash_files_path), [], 'The file was not removed from trash')

    def test_restore_nonexists_file(self):
        file = tempfile.NamedTemporaryFile()
        self.trash.move_to_trash(file.name)
        with self.assertRaises(RMException):
            self.trash.restore(os.path.basename(file.name) + 'x')

    def test_autoclear(self):
        files = [tempfile.NamedTemporaryFile(bufsize=0) for i in range(4)]

        self.trash.maxsize = 90

        files[0].write('test' * 500)
        files[2].write('test' * 500)

        self.trash.move_to_trash(files[0].name)
        self.trash.move_to_trash(files[1].name)

        time.sleep(2)

        self.trash.move_to_trash(files[2].name)
        self.trash.move_to_trash(files[3].name)

        self.trash.auto_clear()

        self.assertFalse(os.path.exists(os.path.join(
                    self.trash.trash_files_path, os.path.basename(files[2].name))),
            'Auto deleting by size is not working')

        self.trash.maxsize = None
        self.trash.maxtime = 2

        self.trash.auto_clear()
        self.assertFalse(os.path.exists(os.path.join(
            self.trash.trash_files_path, os.path.basename(files[1].name))),
            'Auto deleting by time is not working')

        self.assertTrue(os.path.exists(os.path.join(
            self.trash.trash_files_path, os.path.basename(files[3].name))),
            'Removing wrong file')


if __name__ == "__main__":
    unittest.main()