import os
import os.path
import unittest
import shutil
import tempfile

import src.config as cf
import src.smrm as smrm
from src.trash import Trash
from src.smrmexceptions import *


class TestRm(unittest.TestCase):
    def setUp(self):
        config = cf.get_default()
        trash_path = tempfile.mkdtemp()
        config.trash_path = trash_path
        self.trash = Trash(trash_path)

    def tearDown(self):
        shutil.rmtree(self.trash.trash_path)

    def test_rmfile(self):
        file_1 = tempfile.NamedTemporaryFile()
        file_2 = tempfile.NamedTemporaryFile()

        print(file_1.name)
        print(file_2.name)

        smrm.remove(file_1.name, self.trash)
        smrm.remove_file(file_2.name, self.trash)

        self.assertFalse(os.path.exists(file_1.name),'File remove is not working')
        self.assertFalse(os.path.exists(file_2.name),'File remove is not working')

    def test_rmdir(self):
        dir_path_1 = tempfile.mkdtemp()
        dir_path_2 = tempfile.mkdtemp()

        smrm.remove(dir_path_1, self.trash)
        smrm.remove_directory(dir_path_2, self.trash)

        self.assertFalse(os.path.exists(dir_path_1), 'Directory remove is not working')
        self.assertFalse(os.path.exists(dir_path_2), 'Directory remove is not working')

        dir_path = tempfile.mkdtemp()
        file = tempfile.NamedTemporaryFile(dir=dir_path)

        with self.assertRaises(RMException):
            smrm.remove_directory(dir_path, self.trash)

        shutil.rmtree(dir_path)

    def test_remove_tree(self):
        dir_path = tempfile.mkdtemp()
        file_in_dir = tempfile.NamedTemporaryFile(dir=dir_path)
        smrm.remove(dir_path, self.trash)

        self.assertFalse(os.path.exists(dir_path), 'Tree remove is not working')

    def test_remove_by_regex(self):
        dir_path = tempfile.mkdtemp()
        sub_dir_path = tempfile.mkdtemp(dir=dir_path)

        file_1 = tempfile.NamedTemporaryFile(prefix='Test', dir=dir_path)
        file_2 = tempfile.NamedTemporaryFile(prefix='False', dir=dir_path)
        file_3 = tempfile.NamedTemporaryFile(prefix='Test', dir=sub_dir_path)

        smrm.remove_by_regex(dir_path, 'Test.*', self.trash)

        self.assertFalse(os.path.exists(file_1.name), 'The file was not removed')
        self.assertFalse(os.path.exists(file_3.name), 'The file was not removed')
        self.assertTrue(os.path.exists(file_2.name), 'The wrong file was removed')

        shutil.rmtree(dir_path)


if __name__ == '__main__':
    unittest.main()
