import os
import os.path
import json
import ConfigParser

from smrmexceptions import *

_possible_choice_policy = ('select', 'cancel', 'replace')


def get_default():
    class Config(object):
        def __init__(self):
            self.trash_path = os.path.expanduser('~/trash')
            self.choice_policy = 'select'
            self.trash_policies = {
                'maxsize': 0,
                'maxtime': 0,
                'given_space': 0
            }

    return Config()


def get_by_user():
    path = os.path.expanduser('~/.trashconfig')

    if os.path.exists(path):
        config = get_by_file(path)
    else:
        config = get_default()
        save(config, path)

    return config


def get_by_file(path, form='config'):
    if form == 'json':
        data = load_json(path)
    elif form == 'config':
        data = load_config(path)

    config = get_default()
    for key, value in data.items():
        setattr(config, key, value)

    if config.choice_policy not in _possible_choice_policy:
        raise RMException('Incorrect choice policy')

    return config


def save(config, path, form='config'):
    data = {}

    for field in dir(config):
        if not field.startswith('__'):
            data[field] = getattr(config, field)

    if form == 'json':
        save_json(data, path)
    elif form == 'config':
        save_config(data, path)


def load_json(path):
    _is_valid(path)
    decoder = json.JSONDecoder()
    with open(path, 'r') as config_file:
        data = decoder.decode(config_file.read())

    return data


def save_json(data, path):
    if os.path.exists(path):
        if not os.path.isfile(path):
            raise RMException(path + ' is not file')
        if not os.access(path, os.W_OK):
            raise RMException(path + ' Permission denied')

    encoder = json.JSONEncoder(indent=4)
    data = encoder.encode(data)

    with open(path, 'w') as config_file:
        config_file.write(data)


def load_config(path):
    _is_valid(path)
    data = {}

    parser = ConfigParser.ConfigParser()
    parser.read(path)

    for section in parser.sections():
        if section == 'config':
            for option in parser.options(section):
                data[option] = parser.get(section, option)

                if data[option].isdigit():
                    data[option] = int(data[option])
        else:
            data[section] = {}
            for option in parser.options(section):
                data[section][option] = parser.get(section, option)

                if data[section][option].isdigit():
                    data[section][option] = int(data[section][option])

    return data


def save_config(data, path):
    if os.path.exists(path):
        if not os.path.isfile(path):
            raise RMException(path + ' is not file')
        if not os.access(path, os.W_OK):
            raise RMException(path + ' Permission denied')

    parser = ConfigParser.ConfigParser()
    parser.add_section('config')

    for key in data.keys():
        if isinstance(data[key], dict):
            parser.add_section(key)
            for subkey in data[key].keys():
                parser.set(key, subkey, data[key][subkey])
        else:
            parser.set('config', key, data[key])

    with open(path, 'w') as config_file:
        parser.write(config_file)


def _is_valid(path):
    if not os.path.exists(path):
        raise RMException(path + ' File not found')
    if not os.path.isfile(path):
        raise RMException(path + ' is not file')
    if not os.access(path, os.R_OK):
        raise RMException(path + ' Permission denied')
