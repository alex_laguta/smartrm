import uuid
from datetime import datetime


class Transmission(object):
    def __init__(self):
        self.id = uuid.uuid4()
        self.date = datetime.now()
        self.filenames = list()
        self.files = list()
        self.trash_path = None

    def add_file(self, file, name_in_trash):
        self.files.append(file)
        self.filenames.append(name_in_trash)

    def remove_file(self, name):
        index = -1
        for i in range(len(self.filenames)):
            if self.filenames[i] == name:
                index = i
                break
        self.filenames.pop(index)
        self.files.pop(index)

    def show(self):
        print 'ID:', self.id
        print 'Time:', datetime.strftime(self.date, '%H:%M:%S %d.%m.%y')
        print 'Files:'
        for name in self.filenames:
            print((' ' * 5) + name)
