class FileNotFoundError(OSError):
    def __init__(self, filename=None):
        self.filename = filename


class PermissionError(OSError):
    def __init__(self, filename):
        self.filename = filename


class DirectoryIsNotEmpty(OSError):
    def __init__(self, filename):
        self.filename = filename


class RMException(Exception):
    def __init__(self, message):
        self.message = message
