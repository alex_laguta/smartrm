# README #

### What is this repository for? ###

SmartRM is a tool for removing your files to the trash and restoring them from the trash in a smart way (using different policies, regular expressions and a customization of the trash). It's an improved version of linux built-in remove function.
Version: 1.0

### How do I get set up? ###

To install use the following command:
$ python setup.py install

After that you need to use the word SmartRM along with the arguments and files' paths to proceed the actions.

### Who do I talk to? ###

Alex Laguta
e-mail: lehalag@gmail.com